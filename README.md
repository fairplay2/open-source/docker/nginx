# Nginx 1.25.1

This Nginx version add `headers-more-nginx-module` this module allows you to add, set, or clear any output or input header that you specify. for more information visit [this link](https://github.com/openresty/headers-more-nginx-module)

## Installation

Just need to add registry.gitlab.com/fairplay2/open-source/docker/nginx:1.25.1into your dockerfile file.

```docker
FROM registry.gitlab.com/fairplay2/open-source/docker/nginx:1.25.1
```

Then just copy your nginx configuration file to `/etc/nginx/conf.d/default.conf` and copy your html files to `/usr/share/nginx/html` if apply.

## Example
```docker
FROM registry.gitlab.com/fairplay2/open-source/docker/nginx:1.25.1

COPY ./src /usr/share/nginx/html
COPY ./deployment/default.conf /etc/nginx/conf.d/default.conf
```

## Important folders and files
- `/usr/share/nginx/html`
- `/etc/nginx/conf.d/default.conf`

## Maintainers
<table>
    <tr>
        <td align="center">
            <a href="https://gitlab.com/dharwinfairplay">
                <img src="https://gitlab.com/uploads/-/system/user/avatar/12814508/avatar.png?width=400" width="100px;" alt="Dharwin Perez"/>
                <br />
                <sub>
                    <b>Dharwin Perez</b>
                </sub>
            </a>
        </td>
    </tr>
</table>

## Contributors ✨
<table>
    <tr>
        <!-- <td align="center">
            <a href="https://gitlab.com/dharwinfairplay">
                <img src="https://gitlab.com/uploads/-/system/user/avatar/12814508/avatar.png?width=400" width="100px;" alt="Dharwin Perez"/>
                <br />
                <sub>
                    <b>Dharwin Perez</b>
                </sub>
            </a>
        </td> -->
    </tr>
</table>