
# Use a base image
FROM alpine
ARG NGINX_VERSION
ARG HEADERS_MORE_VERSION

# Install depdencies for the compiler
RUN apk add --no-cache --virtual .build-deps \
                gcc \
                libc-dev \
                make \
                openssl-dev \
                pcre2-dev \
                zlib-dev \
                linux-headers \
                libxslt-dev \
                gd-dev \
                geoip-dev \
                libedit-dev \
                bash \
                alpine-sdk \
                findutils

# Define tmp as workdir
WORKDIR /tmp

# Download last version of nginx
ENV NGINX_URL="https://nginx.org/download/nginx-${NGINX_VERSION}.tar.gz"
ENV HEADERS_MORE_URL="https://github.com/openresty/headers-more-nginx-module/archive/refs/tags/v${HEADERS_MORE_VERSION}.tar.gz"

RUN echo ${NGINX_URL}
RUN wget ${NGINX_URL} \
    && tar -xzvf nginx-${NGINX_VERSION}.tar.gz \
    && wget ${HEADERS_MORE_URL} \
    && tar -xzvf "v${HEADERS_MORE_VERSION}.tar.gz"

# Compile nginx with headers-moder module
RUN cd nginx-${NGINX_VERSION} \
    && ./configure \
        --with-debug \
        --prefix=/usr/share/nginx \
        --sbin-path=/usr/sbin/nginx \
        --conf-path=/etc/nginx/nginx.conf \
        --pid-path=/run/nginx.pid \
        --lock-path=/run/lock/subsys/nginx \
        --error-log-path=/var/log/nginx/error.log \
        --http-log-path=/var/log/nginx/access.log \
        --with-http_gzip_static_module \
        --with-http_stub_status_module \
        --with-http_ssl_module \
        --with-pcre \
        --with-http_image_filter_module \
        --with-file-aio \
        --with-ipv6 \
        --with-http_dav_module \
        --with-http_flv_module \
        --with-http_mp4_module \
        --with-http_gunzip_module \
        --add-dynamic-module="/tmp/headers-more-nginx-module-${HEADERS_MORE_VERSION}" \
    && make && make install