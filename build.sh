#!/bin/bash

# Get old version for be used as cache
docker pull $CI_REGISTRY_IMAGE:"${NGINX_VERSION}" || true

# Build it from cache
docker build --cache-from $CI_REGISTRY_IMAGE:"${NGINX_VERSION}" \
--build-arg="NGINX_VERSION=${NGINX_VERSION}" \
--build-arg="HEADERS_MORE_VERSION=${HEADERS_MORE_VERSION}"  \
--tag $CI_REGISTRY_IMAGE:"${NGINX_VERSION}" \
-f $DOCKER_FILE .

# Push images
docker push $CI_REGISTRY_IMAGE:"${NGINX_VERSION}"
